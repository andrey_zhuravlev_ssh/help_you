# -*- coding: utf-8 -*-
from pynput import keyboard
from threading import Thread
import time


class KeyboardThread(Thread):
    """
    Mouse control thread
    """

    def __init__(self):
        """Инициализация потока"""
        Thread.__init__(self)
        self.flag_end = False
        self.flag_enter = False
        self.time_before = 0
        self.f = open("data/out_keyboard.csv",'w+')

    def get_end(self):
        return self.flag_end

    def get_enter(self):
        return self.flag_enter

    def on_press(self, key):
        pass

    def on_release(self, key):
        print('{0} released'.format(key))

        curr_time = time.time()
        if self.time_before > 0:
            delta_time = curr_time - self.time_before
            self.f.write(str(delta_time)+";")
        self.time_before = curr_time

        if key == keyboard.Key.esc:
            self.flag_end = True

        if key == keyboard.Key.enter:
            self.flag_enter = True


    def stop(self):
        self.f.close()
        self.listener.stop()

    def run(self):
        """Запуск потока"""
        # Collect events until released
        with keyboard.Listener(
                on_press=self.on_press,
                on_release=self.on_release) as self.listener:
            self.listener.join()

if __name__ == "__main__":
    keyboard_thread = KeyboardThread()
    keyboard_thread.start()
    time.sleep(50)
    keyboard_thread.stop()