

from PyQt5.QtWidgets import *
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap
# from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtCore import *


import numpy as np
import cv2

s = str("test1.html")
s2 = str("test2.html")

onOff = True

WIN_WIDTH = 700
WIN_HEIGTH = 500

TEXT_USER_TIMEOUT = 'Отдохните'
TEXT_USER_WAKE_UP = 'Проснитесь'

from sandbox.help_you_window import show_window
from sandbox.help_you_sound import sound_wake_up


COUNT_FRAME_OUT_FACE = 24
COUNT_FRAME_OUT_EYES = 24

# fname = '/home/andrey/PycharmProjects/help_you/Cascades/haarcascade_frontalface_default.xml'
# eyeCascade = cv2.CascadeClassifier('Cascades/haarcascade_eye.xml')
#
#
# faceCascade = cv2.CascadeClassifier(fname)
faceCascade = cv2.CascadeClassifier('/home/andrey/PycharmProjects/help_you/Cascades/haarcascade_frontalface_default.xml')
eyeCascade = cv2.CascadeClassifier('/home/andrey/PycharmProjects/help_you/Cascades/haarcascade_eye.xml')

cap = cv2.VideoCapture(0)
cap.set(3,640) # set Width
cap.set(4,480) # set Height

counter_frame_face = 0
counter_frame_eyes = 0
# show_window(TEXT_USER_TIMEOUT)
# exit(2)

while True:
    ret, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(20, 20)
    )

    # блок контроля лица пользователя (наличия)
    if not len(faces):
        counter_frame_face += 1
        if counter_frame_face == COUNT_FRAME_OUT_FACE:
            show_window(TEXT_USER_TIMEOUT)
    else:
        counter_frame_face = 0
    # ========================================

    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]
        roi_color = img[y:y + h, x:x + w]

        eyes = eyeCascade.detectMultiScale(
            roi_gray,
            scaleFactor=1.5,
            minNeighbors=10,
            minSize=(5, 5),
        )
        # блок контроля глаз пользователя (наличия)
        if not len(eyes):
            counter_frame_eyes += 1
            if counter_frame_eyes == COUNT_FRAME_OUT_EYES:
                show_window(TEXT_USER_WAKE_UP)
                # sound_wake_up()
        else:
            counter_frame_eyes = 0

        # ========================================

        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

    cv2.imshow('video',img)

    k = cv2.waitKey(30) & 0xff
    if k == 27: # press 'ESC' to quit
        break


cap.release()
cv2.destroyAllWindows()