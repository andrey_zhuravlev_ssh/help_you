from PyQt5.QtWidgets import *
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap
# from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtCore import *

import numpy as np
import cv2

s = str("test1.html")
s2 = str("test2.html")

onOff = True

WIN_WIDTH = 500
WIN_HEIGHT = 100

WIN_HEIGHT_MENUBAR = 50

LABEL_BUTTON = 'Хорошо'

class MainWindow(QWidget):
    def __init__(self, text, text_button, parent=None):
        QWidget.__init__(self, parent)
        # WIN_WIDTH = QDesktopWidget().screenGeometry().width()
        # WIN_HEIGTH = QDesktopWidget().screenGeometry().height()
        self.resize(WIN_WIDTH, WIN_HEIGHT)
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setWindowFlag(Qt.WindowStaysOnTopHint)

        # self.showMaximized()
        # background - color: darkgray;
        # background - image: url(fon.png);
        self.setStyleSheet("""
                    background-color: ligth gray;
                    """)

        self.label = QLabel(text)
        # self.label.setAlignment(Qt.AlignCenter)
        self.label.setStyleSheet("QLabel {font-size:30px; text-align: center; color: green; background-color: transparent; border: 0;}")

        btn = QPushButton(text_button, self)
        btn.setStyleSheet("QPushButton {background-color: rgb(51,122,183); color: White; border-radius: 4px;height: 40px;}"
                              "QPushButton:pressed {background-color:rgb(31,101,163) ; }")
        btn.resize(btn.sizeHint())
        btn.move(10, 370)

        grid = QGridLayout()
        grid.addWidget(self.label, 0, 0)
        grid.addWidget(btn, 0, 1)
        self.setLayout(grid)
        btn.clicked.connect(self.clickbtn)  # Кнопка
        self.move((QDesktopWidget().screenGeometry().width() - WIN_WIDTH),
                  (QDesktopWidget().screenGeometry().height() - WIN_HEIGHT - WIN_HEIGHT_MENUBAR))
        self.show()

    def clickbtn(self):
        self.close()

    def set_text(self, text):
        self.label.setText(text)

def show_window(text):
    app = None
    if not QApplication.instance():
        app = QApplication([])
    dlg = MainWindow(text, LABEL_BUTTON)
    if app: app.exec_()