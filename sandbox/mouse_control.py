from pynput import mouse

PIXEL_MOUSE_MIN = 10

COUNTER_MOUSE_MAX = 50

x_before = 0
y_before = 0
counter_mouse_min = 0
counter_mouse_bad = 0

def on_move(x, y):
    global x_before
    global y_before
    global counter_mouse_min
    global counter_mouse_bad
    # print('Pointer moved to {0}'.format(
    #     (x, y)))
    #
    # print('x_before Pointer moved to {0}'.format(
    #     (x_before, y_before)))
    #
    #
    # print(abs(x-x_before))
    # print(abs(y-y_before))

    if abs(x-x_before) < PIXEL_MOUSE_MIN or abs(y-y_before) < PIXEL_MOUSE_MIN:
        counter_mouse_min += 1
        print("counter_mouse_min {}".format(counter_mouse_min))
        if counter_mouse_min > COUNTER_MOUSE_MAX:
            print("BAD!")
        else:
            counter_mouse_bad = 0
    else:
        counter_mouse_min = 0
        x_before = x
        y_before = y


def on_click(x, y, button, pressed):
    pass


def on_scroll(x, y, dx, dy):
    pass

# Collect events until released
with mouse.Listener(
    on_move=on_move,
    on_click=on_click,
    on_scroll=on_scroll) as listener:
    listener.join()