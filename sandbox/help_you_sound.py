import pyglet
SOUND_WAKE_UP = '/home/andrey/PycharmProjects/help_you/audio/sound_5s.mp3'

def play(file_name):
    player = pyglet.media.Player()
    source = pyglet.media.load(file_name)
    player.queue(source)

    player.play()

    def update(dt):

        if not player.playing:
            # Отпишем функцию, иначе при повторном вызове, иначе
            # будет двойной вызов при следующем воспроизведении
            pyglet.clock.unschedule(update)
            pyglet.app.exit()

    # Every 500 ms / 0.5 sec
    pyglet.clock.schedule_interval(update, 0.5)
    pyglet.app.run()

def sound_wake_up():
    play(SOUND_WAKE_UP)

