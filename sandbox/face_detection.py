import numpy as np
import cv2
import os

COUNT_FRAME_OUT = 24

fname = '/home/andrey/PycharmProjects/help_you/Cascades/haarcascade_frontalface_default.xml'

print(os.path.isfile(fname))
print("START")


faceCascade = cv2.CascadeClassifier(fname)

cap = cv2.VideoCapture(0)
cap.set(3,640) # set Width
cap.set(4,480) # set Height

counter_frame = 0

while True:
    ret, img = cap.read()
    # img = cv2.flip(img, -1)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(20, 20)
    )


    if not len(faces):
        counter_frame += 1
        if(counter_frame == COUNT_FRAME_OUT):
            print("Where you?")
    else:
        counter_frame = 0

    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]

    cv2.imshow('video',img)

    k = cv2.waitKey(30) & 0xff
    if k == 27: # press 'ESC' to quit
        break

cap.release()
cv2.destroyAllWindows()