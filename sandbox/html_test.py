# import htmlPy
# import os
#
# app = htmlPy.AppGUI(title=u"htmlPy Quickstart", maximized=True)
#
# app.template_path = os.path.abspath(".")
# app.static_path = os.path.abspath(".")
#
# app.template = ("index.html", {"username": "htmlPy_user"})
#
# app.start()

# !/usr/bin/python

import PyQt5
from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtWebEngineWidgets import QWebEngineView as QWebView,QWebEnginePage as QWebPage
from PyQt5.QtWebEngineWidgets import QWebEngineSettings as QWebSettings
import sys
from optparse import OptionParser


# class MyBrowser(QWebPage):
#     ''' Settings for the browser.'''
#
    # def userAgentForUrl(self, url):
    #     ''' Returns a User Agent that will be seen by the website. '''
    #     return "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"


class Browser(QWebView):
    def __init__(self):
        # QWebView
        self.view = QWebView.__init__(self)
        # self.view.setPage(MyBrowser())
        self.setWindowTitle('Loading...')
        self.titleChanged.connect(self.adjustTitle)
        # super(Browser).connect(self.ui.webView,QtCore.SIGNAL("titleChanged (const QString&amp;)"), self.adjustTitle)

    def load(self, url):
        self.setUrl(QUrl(url))

    def adjustTitle(self):
        self.setWindowTitle(self.title())

    def disableJS(self):
        settings = QWebSettings.globalSettings()
        settings.setAttribute(QWebSettings.JavascriptEnabled, False)


app = QApplication(sys.argv)
view = Browser()
view.showMaximized()
view.load("https://pythonspot.com")
app.exec_()