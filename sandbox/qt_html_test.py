#!/usr/bin/python
# -*- coding: utf-8 -*-
import webbrowser
from PyQt5.QtWidgets import *
from PyQt5.QtWebEngineWidgets import *
import requests
import sys
from PyQt5 import QtCore, QtWidgets

# s = str("""<html><head></head><body>Привет мир!!!</body></html>""")
# s2 = str("""<html><head></head><body>Прощай мир!!!</body></html>""")

s = str("test1.html")
s2 = str("test2.html")

onOff = True

WIN_WIDTH = 700
WIN_HEIGTH = 500


def clikbtn():
    MainWindow.view.setHtml(s)
    # Что то должно произойти
#
# def open_html_file(path_html):
#     f = open(path_html).read()
#     return f.read()


class MainWindow(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.resize(WIN_WIDTH, WIN_HEIGTH)
        # self.showMaximized()
        self.view = QWebEngineView(self)
        # mypage = self.view

        mypage = MyPage(self.view)
        self.view.setPage(mypage)
        url = 'http://ya.ru'
        response = requests.get(url)
        mypage.setHtml(response.text)

        combo = QComboBox(self)
        combo.addItems(['s', 's2'])
        # combo.move(0, 470)
        # self.lbl.move(50, 150)
        combo.activated[str].connect(self.onActivated)

        btn = QPushButton('Обовить страницу', self)
        btn.setToolTip('This is a <b>QPushButton</b> widget')
        btn.resize(btn.sizeHint())
        # btn.move(10, 370)

        grid = QGridLayout()
        grid.addWidget(self.view, 0, 0)
        grid.addWidget(combo, 1, 0)
        grid.addWidget(btn, 2, 0)
        self.setLayout(grid)
        btn.clicked.connect(self.clikbtn)  # Кнопка обновления карты
        self.move((QDesktopWidget().screenGeometry().width() - WIN_WIDTH)/2,
                  (QDesktopWidget().screenGeometry().height() - WIN_HEIGTH)/2)
        self.show()

    def clikbtn(self):
        url = 'http://ya.ru'
        response = requests.get(url)
        self.view.setHtml(response.text)
        # self.view.reload()

    def onActivated(self, text):
        if text == "s":
            # self.view.setHtml(s)
            self.view.setHtml(open(s).read())
        elif text == "s2":
            # self.view.setHtml(s2)
            self.view.setHtml(open(s2).read())

            # Что то должно произойти


class MyPage(QWebEnginePage):
    def __init__(self, parent):
        super().__init__(parent)
        # webbrowser.open(url_string)
        # self.in_window = False  # придумал переменную

    def createWindow(self, type):  # которую мы
        # self.in_window = True  # тутже изменяем если просится
        return self  # открытие в новом окне

    def acceptNavigationRequest(self, QUrl, type, isMainFrame):
        url_string = QUrl.toString()
        self.parent().parent().close()

        return True


if __name__ == '__main__':
    app = None
    if not QApplication.instance():
        app = QApplication([])
    dlg = MainWindow()
    if app: app.exec_()

    app = None
    if not QApplication.instance():
        app = QApplication([])
    dlg = MainWindow()
    if app: app.exec_()