from PyQt5.QtWidgets import *
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import *
import webbrowser
from PyQt5.QtWidgets import *
from PyQt5.QtWebEngineWidgets import *

import sys
import os

WIN_ALARM = 1
WIN_GYM_1 = 2
WIN_TIMER = 3
WIN_STARTUP = 4


CAPTION_NEXT = "Следующее упражнение"
CAPTION_END = "Закончить"

# CAPTION_IGNORE = "Игнорировать"
CAPTION_IGNORE = "Вернуться к работе"
CAPTION_CALIBRATE = "Откалибровать заново"

directory = os.path.abspath(os.curdir)+"/"
FILE_HTML_GYM = directory+"html/gym_all.html"

FILE_HTML_ALARM = directory+"html/stron_alarm.html"

FILE_HTML_TIMER = directory+"html/timer_long.html"


FILE_HTML_STARTUP = directory+"html/onboarding.html"

PROGRAMM_NAME = "help_you.py"
CONFIG_FILE = ".help_you"

USER_TIMEOUT_WAIT = 3

WIN_HEIGHT_MENUBAR = 50
WIN_OPACITY = 0.85

MARGIN_BOTTOM_PIXEL = 140
MARGIN_BOTTOM_PIXEL_TIMER = 100



STYLE_BUTTON_1 = ("QPushButton {background-color: transparent; color: white;"
                          "border: 1px solid #EDEDED;"
                          "border-radius: 4px; white; border-radius: 4px;height: 30px;width: 200px;}"
                              "QPushButton:pressed {background-color:#EFEFEF; }")
STYLE_BUTTON_2 = ("QPushButton {background-color: transparent; color: white; border-radius: 4px;border: 0;height: 40px;}"
                              "QPushButton:pressed {background-color:transparent ; }")

app = None

class MainWindow(QWidget):
    def __init__(self, status, parent=None):
        QWidget.__init__(self, parent)
        WIN_WIDTH = QDesktopWidget().screenGeometry().width()
        WIN_HEIGHT = QDesktopWidget().screenGeometry().height()
        WIN_OPACITY = 0.85
        if status == WIN_ALARM:
            caption_1 = CAPTION_IGNORE
            caption_2 = CAPTION_CALIBRATE
            file_html = FILE_HTML_ALARM
        elif status == WIN_GYM_1:
            WIN_WIDTH = 1280
            WIN_HEIGHT = 720
            caption_1 = CAPTION_END
            caption_2 = None
            file_html = FILE_HTML_GYM
            self.move((QDesktopWidget().screenGeometry().width() - WIN_WIDTH) / 2,
                      (QDesktopWidget().screenGeometry().height() - WIN_HEIGHT) / 2)
        elif status == WIN_TIMER:
            caption_1 = CAPTION_IGNORE
            caption_2 = None
            file_html = FILE_HTML_TIMER
        # elif status == WIN_STARTUP:
        else:
            WIN_WIDTH = 768
            WIN_HEIGHT = 408
            WIN_OPACITY = 1
            caption_1 = None
            caption_2 = None
            file_html = FILE_HTML_STARTUP
            self.move((QDesktopWidget().screenGeometry().width() - WIN_WIDTH) / 2,
                      (QDesktopWidget().screenGeometry().height() - WIN_HEIGHT) / 2)
        self.resize(WIN_WIDTH, WIN_HEIGHT)
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setWindowFlag(Qt.WindowStaysOnTopHint)
        if status != WIN_GYM_1:
            self.setAttribute(Qt.WA_TranslucentBackground)

        self.view_web = QWebEngineView(self)

        self.view = MyPage(self.view_web)
        self.view_web.setPage(self.view)

        self.setWindowOpacity(WIN_OPACITY)
        # self.view.setHtml(open(file_html).read())

        self.view.load(QUrl.fromLocalFile(file_html))
        # self.view.load(QUrl("https://ya.ru"))

        self.view_web.resize(WIN_WIDTH, WIN_HEIGHT)
        self.grid = QGridLayout()
        self.grid.addWidget(self.view_web, 0, 0)
        if caption_1:
            btn = QPushButton(caption_1, self)
            btn.setStyleSheet(STYLE_BUTTON_1)
            btn.setCursor(QCursor(Qt.PointingHandCursor))
            btn.resize(btn.sizeHint())
            btn.clicked.connect(self.clickbtn)
            btn.move((WIN_WIDTH - btn.sizeHint().width()) / 2, WIN_HEIGHT - MARGIN_BOTTOM_PIXEL)

        if caption_2:
            btn_2 = QPushButton(caption_2, self)
            btn_2.setStyleSheet(STYLE_BUTTON_2)
            btn_2.setCursor(QCursor(Qt.PointingHandCursor))
            btn_2.resize(btn_2.sizeHint())
            btn_2.clicked.connect(self.clickbtn_2)
            btn_2.move((WIN_WIDTH - btn_2.sizeHint().width()) / 2, WIN_HEIGHT - MARGIN_BOTTOM_PIXEL)
            btn.move((WIN_WIDTH - btn.sizeHint().width()) / 2,
                     WIN_HEIGHT - MARGIN_BOTTOM_PIXEL - btn.sizeHint().height())
        elif caption_1 and status != WIN_GYM_1:
            btn.move((WIN_WIDTH - btn.sizeHint().width()) / 2, WIN_HEIGHT/2 + MARGIN_BOTTOM_PIXEL_TIMER)

    def clickbtn(self):
        #set next
        print("set_next")
        f = open(CONFIG_FILE, "w")
        f.write("ready")
        f.close()
        self.close()
        os.system("pkill python")
        exit(1)


    def clickbtn_2(self):
        # self.close()
        pass




class MyPage(QWebEnginePage):
    def __init__(self, parent):
        super().__init__(parent)

    def createWindow(self, type):
        return self

    def acceptNavigationRequest(self, QUrl, type, isMainFrame):
        url_string = QUrl.toString()
        print(url_string)
        if url_string == "file://localhost/skip" or url_string == "file://localhost/endboard" or url_string == "file://localhost/end":
            self.parent().parent().close()
            f = open(CONFIG_FILE, "w")
            f.write("ready")
            f.close()
            exit(1)

        return True


def show_window_html(status):

    if not QApplication.instance():
        app = QApplication([])
    dlg = MainWindow(status)
    dlg.show()
    if app:
        sys.exit(app.exec_())

if __name__ == "__main__":
    # show_window_html(WIN_STARTUP)
    # show_window_html(WIN_GYM_1)
    show_window_html(WIN_TIMER)

