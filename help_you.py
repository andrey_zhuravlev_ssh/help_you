import cv2
import time
import os
import sys


from help_you_window import show_window
from help_you_win_html import show_window_html
from help_you_sound import sound_wake_up
from mouse_control import MouseThread
from keyboard_control import KeyboardThread
from opencv_control import OpenCVThread

from opencv_control import USER_STATUS_OUT, USER_STATUS_WORKING, USER_STATUS_SLEEP, USER_STATUS_BAD_SEAT

from help_you_win_html import WIN_ALARM, WIN_GYM_1, WIN_TIMER, WIN_STARTUP, CONFIG_FILE, PROGRAMM_NAME

from help_you_window import WIN_ASK_ABOUT_BREAK, WIN_WALK, WIN_GYM_TEXT, WIN_SLEEP

WIN_WIDTH = 700
WIN_HEIGTH = 500

TEXT_USER_TIMEOUT = 'Отдохните'
TEXT_USER_WAKE_UP = 'Проснитесь'

TEXT_USER_MOUSE_BAD = 'Похоже у Вас тремор...'

TIME_BEFORE_WALK = 50*60
TIME_BEFORE_GYM = 25*60

DEBUG = True


DEMO_NUMBER = 1




WAIT_TIME_MIN = 1

DEBUG_PRINT = True


def tech_print(text):
    if DEBUG_PRINT:
        print(text)

class UserControl():

    def __init__(self):
        self.flag_mouse_problem = False
        # self.flag_user_correction = False
        self.flag_user_correction = True

        self.mouse_thread = MouseThread()
        self.mouse_thread.start()


        self.keyboard_thread = KeyboardThread()
        self.keyboard_thread.start()


        self.opencv_thread = OpenCVThread()
        self.opencv_thread.set_debug(DEBUG)
        self.opencv_thread.start()

        self.control()

    def __del__(self):
        config_txt = ""
        if os.path.exists(CONFIG_FILE):
            f = open(CONFIG_FILE, "r+")
            config_txt = f.read()

        if config_txt == "ready":
            directory = os.path.abspath(os.curdir) + "/"
        else:
            tech_print("pkill python")
            os.system("pkill python")

        self.stop()

    def user_wake_up(self):
        """
        event handle 'user sleep'
        :return:
        """

        show_window(TEXT_USER_WAKE_UP)
        # sound_wake_up()

    def user_keyboard_control(self):
        """
        event handle 'user keyboard control' (press Esc)
        :return:
        """
        return self.keyboard_thread.get_end()

    def user_mouse_control(self):
        """
        event handle 'user mouse control'
        :return:
        """
        if not self.flag_mouse_problem:
            self.flag_mouse_problem = self.mouse_thread.get_problem()

            if self.flag_mouse_problem:
                show_window(TEXT_USER_MOUSE_BAD)
                self.flag_mouse_problem = False

    def user_opencv_control(self):
        """
        event handle 'user mouse control'
        :return:
        """
        status = self.opencv_thread.get_status()
        if status != USER_STATUS_WORKING:
            tech_print("status {}".format(status))
        if status == USER_STATUS_OUT:
            show_window(WIN_ASK_ABOUT_BREAK)
            time.sleep(WAIT_TIME_MIN)
        elif status == USER_STATUS_SLEEP:
            show_window(WIN_SLEEP)
            time.sleep(WAIT_TIME_MIN)
        elif status == USER_STATUS_BAD_SEAT:

            show_window_html(WIN_ALARM)



    def control(self):
        """
        main thread - control
        :return:
        """
        self.time_start = time.time()
        self.time_after_walk = self.time_start
        self.show_window_walk = False

        self.time_after_gym = self.time_start
        self.show_window_gym = False

        directory = os.path.abspath(os.curdir) + "/"

        while True:
            delta_time = time.time() - self.time_after_walk
            if delta_time > TIME_BEFORE_WALK:
                show_window(WIN_WALK)
                show_window_html(WIN_TIMER)
                self.time_after_walk = time.time()

            delta_time = time.time() - self.time_after_gym
            if delta_time > TIME_BEFORE_GYM:
                show_window(WIN_GYM_TEXT)
                show_window_html(WIN_GYM_1)

                self.time_after_gym = time.time()


            if self.flag_user_correction and self.keyboard_thread.get_enter():
                self.face = self.opencv_thread.get_info_face()
                tech_print(self.face)
                if not self.face is None:
                    self.opencv_thread.set_face(self.face)
                    self.flag_user_correction = False
                else:
                    tech_print("Error get face")

            if self.user_keyboard_control():
                tech_print("stop")
                self.stop()
                break
            self.user_mouse_control()
            self.user_opencv_control()

            time.sleep(0.1)



    def stop(self):

        self.mouse_thread.stop()

        tech_print("self.mouse_thread.stop()")
        self.keyboard_thread.stop()
        tech_print("self.keyboard_thread.stop()")
        self.opencv_thread.stop()
        tech_print("self.opencv_thread.stop()")
        cv2.destroyAllWindows()
        tech_print("cv2.destroyAllWindows()")



if __name__ == "__main__":
    if len(sys.argv) > 1:
        DEMO_NUMBER = int(sys.argv[1])

    if DEMO_NUMBER == 2:
        TIME_BEFORE_GYM = 5  # Time before walk for demo
        DEBUG = False

    if DEMO_NUMBER == 3:
        TIME_BEFORE_WALK = 5  # Time before walk for demo
        DEBUG = False

    config_txt = ""
    if os.path.exists(CONFIG_FILE):
        f = open(CONFIG_FILE, "r+")
        config_txt = f.read()

    if config_txt == "":
        show_window_html(WIN_STARTUP)

    UserControl()
    os.remove(CONFIG_FILE)


