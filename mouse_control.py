# -*- coding: utf-8 -*-
from pynput import mouse
from threading import Thread

PIXEL_MOUSE_MIN = 10

COUNTER_MOUSE_MAX = 100

x_before = 0
y_before = 0
counter_mouse_min = 0
counter_mouse_bad = 0


class MouseThread(Thread):
    """
    Mouse control thread
    """

    def __init__(self):
        """Инициализация потока"""
        Thread.__init__(self)
        self.flag_problem = False

    def get_problem(self):
        if self.flag_problem:
            self.flag_problem = False
            return True
        return False

    def on_move(self, x, y):
        if not self.flag_problem:
            global x_before
            global y_before
            global counter_mouse_min
            global counter_mouse_bad
            # print('Pointer moved to {0}'.format(
            #     (x, y)))
            #
            # print('x_before Pointer moved to {0}'.format(
            #     (x_before, y_before)))

            # print(abs(x - x_before))
            # print(abs(y - y_before))

            if abs(x - x_before) < PIXEL_MOUSE_MIN or abs(y - y_before) < PIXEL_MOUSE_MIN:
                counter_mouse_min += 1
                if counter_mouse_min > COUNTER_MOUSE_MAX:
                    print("BAD!")
                    self.flag_problem = True
                    counter_mouse_min = 0
            else:
                counter_mouse_min = 0
                x_before = x
                y_before = y

    def on_click(self, x, y, button, pressed):
        pass

    def on_scroll(self, x, y, dx, dy):
        pass

    def stop(self):
        self.listener.stop()

    def run(self):
        """Запуск потока"""
        # Collect events until released
        with mouse.Listener(
                on_move=self.on_move,
                on_click=self.on_click,
                on_scroll=self.on_scroll) as self.listener:
            self.listener.join()
