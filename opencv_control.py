# -*- coding: utf-8 -*-
import cv2
from threading import Thread

DEBUG = True
# DEBUG = False

COUNT_FRAME_OUT_FACE = 50
COUNT_FRAME_OUT_EYES = 50


MAX_PIXELS_OUT = 150
MAX_PIXELS_SIZE = 150


USER_STATUS_WORKING = 0
USER_STATUS_OUT = 1
USER_STATUS_SLEEP = 2
USER_STATUS_BAD_SEAT = 3

class OpenCVThread(Thread):

    def __init__(self):
        """Инициализация потока"""
        Thread.__init__(self)

        self.is_stop = False

        self.face = None

        self.status = USER_STATUS_WORKING


        self.faceCascade = cv2.CascadeClassifier('Cascades/haarcascade_frontalface_default.xml')
        self.eyeCascade = cv2.CascadeClassifier('Cascades/haarcascade_eye.xml')

        self.cap = cv2.VideoCapture(0)
        self.cap.set(3, 640)  # set Width
        self.cap.set(4, 480)  # set Height
        self.debug = True

    def set_debug(self, debug):
        self.debug = debug

    def get_status(self):
        return self.status

    def set_face(self, face):
        self.face = face

    def get_info_face(self):
        if len(self.faces) != 1:
            return None
        return self.faces[0]

    def run(self):
        counter_frame_face = 0
        counter_frame_eyes = 0
        while True:
            ret, img = self.cap.read()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            self.faces = self.faceCascade.detectMultiScale(
                gray,
                scaleFactor=1.2,
                minNeighbors=5,
                minSize=(20, 20)
            )


            # блок контроля лица пользователя (наличия)
            if not len(self.faces):
                counter_frame_face += 1
                if counter_frame_face == COUNT_FRAME_OUT_FACE:
                    self.status = USER_STATUS_OUT
            else:
                if len(self.faces) == 1 and not (self.face is None):
                    (x, y, w, h) = self.faces[0]
                    (xe, ye, we, he) = self.face
                    if not (abs(xe-x) < MAX_PIXELS_OUT and abs(ye-y) < MAX_PIXELS_OUT and abs(we-w) < MAX_PIXELS_SIZE and abs(he-h) < MAX_PIXELS_SIZE):
                        self.status = USER_STATUS_BAD_SEAT

                if self.status != USER_STATUS_BAD_SEAT:
                    self.status = USER_STATUS_WORKING
                counter_frame_face = 0
            # ========================================
            for (x, y, w, h) in self.faces:
                cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
                roi_gray = gray[y:y + h, x:x + w]
                roi_color = img[y:y + h, x:x + w]

                eyes = self.eyeCascade.detectMultiScale(
                    roi_gray,
                    scaleFactor=1.5,
                    minNeighbors=10,
                    minSize=(5, 5),
                )
                # блок контроля глаз пользователя (наличия)
                if not len(eyes):
                    counter_frame_eyes += 1
                    if counter_frame_eyes == COUNT_FRAME_OUT_EYES:
                        self.status = USER_STATUS_SLEEP
                        print("status sleep {}".format(self.status))
                else:
                    counter_frame_eyes = 0
                    self.status = USER_STATUS_WORKING

                # ========================================

                for (ex, ey, ew, eh) in eyes:
                    cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
            if self.debug:
                cv2.imshow('video',img)
            k = cv2.waitKey(30) & 0xff

            if self.is_stop:
                break
            # if k == 27: # press 'ESC' to quit
            #     break
        self.cap.release()
        cv2.destroyAllWindows()

    def stop(self):
        self.is_stop = True
