from PyQt5.QtWidgets import *
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import *

import time

onOff = True

WIN_ASK_ABOUT_BREAK = 1
WIN_WALK = 2
WIN_GYM_TEXT = 3
WIN_SLEEP = 4

# USER_TIMEOUT_WAIT = 10*60
USER_TIMEOUT_WAIT = 3


class MainWindow(QWidget):
    def __init__(self, notification, typeNotification, parent=None):
        QWidget.__init__(self, parent)

        # Указать вариант окна
        # typeNotification = 'mini'
        # notification = 'backMessage'

        # Все варианты уведомлений для информации
        # typeNotificationArr = {'mini', 'standard', 'great'}
        # notificationArr = {
        #     'goodJob', #great
        #     'lightUp', #standard
        #     'gymnastics', #standard
        #     'walk', #standard
        #     'askAboutBreak', #standard
        #     'sitCommand', #mini
        #     'backMessage' #mini
        # }

        self.notification = notification
        # self.show_timer = False

        if typeNotification == 'great':
            WIN_WIDTH = 500
            WIN_HEIGHT = 150
        if typeNotification == 'standard':
            WIN_WIDTH = 500
            WIN_HEIGHT = 150
        if typeNotification == 'mini':
            WIN_HEIGHT = 75
            WIN_WIDTH = 250 if notification == 'sitCommand' else 375

        WIN_HEIGHT_MENUBAR = 50

        LABEL_TITLE_GYM = ''
        LABEL_CAPTION = ''
        LABEL_BUTTON = ''
        LABEL_BUTTON_2 = ''

        FON_IMAGE = 'images/fon-message-2.png'

        if typeNotification == 'standard':
            if notification == 'lightUp':
                LABEL_TITLE_GYM = 'Вы очень устали?'
                # LABEL_CAPTION = 'Работа в темноте очень плохо  влияет на ваше зрение.'
                LABEL_CAPTION = 'Вам стоит на сегодня закончить'
                LABEL_BUTTON = 'Игнорировать'
                LABEL_BUTTON_2 = 'Выкл. компьютер'

            if notification == 'gymnastics':
                LABEL_TITLE_GYM = '5 минут для гимнастики!'
                LABEL_CAPTION = 'Вашим глазам и шее нужно  отдохнуть. Начнём через минуту'
                LABEL_BUTTON = 'Начать сейчас'
                LABEL_BUTTON_2 = 'Отложить на 10 мин.'

            if notification == 'walk':
                LABEL_TITLE_GYM = '15 минут для прогулки'
                LABEL_CAPTION = 'Вам необходимо размяться.\nВстаньте и прогуляйтесь.'
                LABEL_BUTTON = 'Начать сейчас'
                LABEL_BUTTON_2 = 'Отложить на 10 мин.'

            if notification == 'askAboutBreak':
                LABEL_TITLE_GYM = 'Кажется, вы отходили'
                LABEL_CAPTION = 'Вы отдыхали или  это было по работе?'
                LABEL_BUTTON = 'По работе'
                LABEL_BUTTON_2 = 'Отдыхал'

        if typeNotification == 'great':
            FON_IMAGE = 'images/fon-message-great.png'

            if notification == 'goodJob':
                LABEL_TITLE_GYM = 'Хорошая работа!'
                LABEL_CAPTION = 'Вы получили достижение «Итальянская забастовка» за то что всю неделю выполняли инструкции.'

        if typeNotification == 'mini':
            if notification == 'sitCommand':
                FON_IMAGE = 'images/fon-black-short.png'
                LABEL_CAPTION = 'Сядьте прямо! 😠'

            if notification == 'backMessage':
                FON_IMAGE = 'images/fon-image-long.png'
                LABEL_CAPTION = 'Ваша спина скажет вам спасибо! 👍'

        STYLE_CAPTION = ("QLabel {font-family: Roboto;"
                         "top: 20px;"
                         "color: #fff;"
                         "font-weight: 600;"
                         "font-size:14px;"
                         "background-color: transparent; "
                         "border: 0;}") \
        if typeNotification == 'mini' else ("QLabel {font-family: Roboto;"
                                                "top: 20px;"
                                                "font-size:14px;"
                                                "background-color: transparent; "
                                                "border: 0;}")

        text = LABEL_TITLE_GYM
        text_caption = LABEL_CAPTION
        text_button = LABEL_BUTTON
        text_button_2 = LABEL_BUTTON_2


        self.resize(WIN_WIDTH, WIN_HEIGHT)
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setWindowFlag(Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.load_image(FON_IMAGE)
        self.label = QLabel(text)
        self.label.setStyleSheet("QLabel {font-family: Roboto;"
                                 "font-style: normal;"
                                 "margin-top: -10px;"
                                 "font-weight: 505;"
                                 "font-size:14px;"
                                 "background-color: transparent; "
                                 "border: 0;}")

        self.label_caption = QLabel(text_caption)
        self.label_caption.setStyleSheet(STYLE_CAPTION)

        btn = QPushButton(text_button, self)

        # Стили для кнопок
        if typeNotification == 'great':
            btn.setStyleSheet("QPushButton {background-color: transparent; color: black; border-radius: 4px;border: 0;height: 40px;}"
                              "QPushButton:pressed {background-color:rgb(31,101,163) ; }")
        if typeNotification == 'standard': # Форма great
            btn.setStyleSheet("QPushButton {background-color: #EDEDED; color: black; border-radius: 4px;border: 0;height: 40px;}"
                              "QPushButton:pressed {background-color:rgb(31,101,163) ; }")
        if typeNotification == 'mini': # Форма great
            btn.setStyleSheet("QPushButton {background-color: rgba(0, 0, 0, 0);}")

        btn_2 = QPushButton(text_button_2, self)
        btn_2.setStyleSheet("QPushButton {background-color: transparent; color: Black; border-radius: 4px;border: 0;height: 40px;}"
                              "QPushButton:pressed {background-color:transparent ; }")
        btn_2.setCursor(QCursor(Qt.PointingHandCursor))
        btn.resize(btn.sizeHint())
        btn.move(10, 370)

        grid = QGridLayout()
        grid.setSpacing(20)
        grid.setVerticalSpacing(0)

        if typeNotification == 'mini':
            grid.setContentsMargins(20, 10, 0, 0)
        if typeNotification == 'great':
            grid.setContentsMargins(70, 0, 0, 35)
        if typeNotification == 'standard':
            grid.setContentsMargins(70, 10, 20, 50)

        grid.addWidget(self.label, 0, 0)

        if typeNotification == 'mini':
            grid.addWidget(self.label_caption, 0, 0)
        else:
            grid.addWidget(self.label_caption, 1, 0)

        grid.addWidget(btn, 0, 1)
        grid.addWidget(btn_2, 1, 1)
        self.setLayout(grid)
        btn.clicked.connect(self.clickbtn)
        btn_2.clicked.connect(self.clickbtn_2)
        self.move((QDesktopWidget().screenGeometry().width() - WIN_WIDTH),
                  (QDesktopWidget().screenGeometry().height() - WIN_HEIGHT - WIN_HEIGHT_MENUBAR))

    # def get_show_timer(self):
    #     return self.show_timer

    def clickbtn(self):
        if self.notification == "walk":
            self.show_timer = True
        self.close()


    def clickbtn_2(self):
        if self.notification == "askAboutBreak":
            print('askAboutBreak')
        elif self.notification == 'lightUp':
            print('shutdown')
            # import os
            # os.system('systemctl poweroff')
        else:
            self.hide()
            time.sleep(USER_TIMEOUT_WAIT)
            self.show()

    def load_image(self, file_name):
        pixmap = QPixmap(file_name)

        self.label = QLabel(self)
        self.label.setPixmap(pixmap)
        self.label.resize(pixmap.width(), pixmap.height())
        # self.label.setStyleSheet("QLabel {box-shadow: 0px 10px black}")
        self.resize(pixmap.width(), pixmap.height())

    def set_text(self, text):
        self.label.setText(text)

def show_window(status):
    app = None
    if not QApplication.instance():
        app = QApplication([])

    if status == WIN_ASK_ABOUT_BREAK:
        dlg = MainWindow('askAboutBreak', 'standard')
    elif status == WIN_WALK:
        dlg = MainWindow('walk', 'standard')
    elif status == WIN_GYM_TEXT:
        dlg = MainWindow('gymnastics', 'standard')
    elif status == WIN_SLEEP:
        dlg = MainWindow('lightUp', 'standard')
    dlg.show()
    if app: app.exec_()


if __name__ == "__main__":
    show_window(WIN_SLEEP)